# Contributing
-----------------------

The Migration Factory make use of ___Git Flow___. When contributing to this
repository, please ensure that you follow the appropriate ___Git Flow Process___.

For additional information please see:
* [Confluence - Git Flow](https://confluence.aviva.co.uk/display/CMWR/Git+Flow)
* [Attlassian - Git Flow](https://www.atlassian.com/git/tutorials/comparing-workflows)

# Versioning
-----------------------
The Migration Factory make use of [Semantic Versioning](https://semver.org/)

# Standards & Ways of Working
-----------------------
* ___Merge Frequently___, it is much easier and quicker for people to review
  concise code.
* ___Lint___ your code before you commit
* Communicate with other developers working on the same repository
* Clearly document nuances and edge cases of the code within the repository,
  essentially anything that isn't self explanatory within the code.

# Pull Request Process
-----------------------
## Pull Request to Develop
1. Update the README.md
2. Raise a pull request to develop
3. Ensure that your pull request is reviewed by at least two people
4. Resolve any issues raised during the pull request review
5. Merge to the develop branch and ensure that you delete your feature branch

## Pull Request to master
1. Create a ___Release Branch___
2. Update the CHANGELOG.md
3. Create a pull request
4. Ensure that your pull request is reviewed by at least two people
5. Resolve any issues raised during the pull request review
6. Merge to the master branch
7. Tag the master branch with the new release version
