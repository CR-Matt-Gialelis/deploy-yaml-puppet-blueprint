# Fucntions
Param (
  [Parameter(Mandatory=$False)]  [String] $Proxy  =   $null
)
if($proxy){
Set-AWSProxy -Hostname $proxy -Port 80
}

Import-Module AWSPowerShell


function Online-AllDisks($diskset) {
    # Detach, then reattach, disks in presented diskset
    $update=$false
    foreach ($d in $diskset) {
        if ( $d.OperationalStatus -eq "Offline" ) {
            $dnum=$d.Number
            Write-Host "Bringing disk $dnum online"
            Set-Disk -number $dnum -isOffline $false
            $update=$true
        }
    }
    if ($update) {
        Update-HostStorageCache
    }
}

function Get-NonBootDisks
{
   Update-HostStorageCache
   $AllDisks = Get-Disk | Where PartitionStyle -ne 'MBR'
   Online-AllDisks($AllDisks)
   $AllDisks
}


function Get-VolumeTag{
  	param([String] $VolumeId,
          [String] $Tag
          )
          Get-EC2Tag | Where ResourceId -eq $VolumeId | Where Key -eq $Tag
}

function ForceReadWrite-Disks($diskset) {
    # All non-boot disks become read-write
    $diskset | % { set-Disk -number $_.Number -isReadOnly $false}
}

function Init-Disk{
  param([String] $Disknum )
      	Initialize-Disk $Disknum -PartitionStyle GPT -PassThru
}


function Clean-VolumeIds{
	#Used to clean the Volume Id's of Instances that use NVME type EBS volumes due to the the Volume ID from Get-Disk Contianing extra characters
	Param([String] $Volumeid)

		if($Volumeid -like '*_*'){
			$Volumeid.Substring(0, $Volumeid.IndexOf('_'))
		}
		else{
			$Volumeid
		}
	}


$Disks = Get-NonBootDisks

Foreach ($Disks in $Disks)
{
  $VolumeId = Clean-VolumeIds($Disks.SerialNumber.Insert(3,'-'))
  $VolumeID
  Init-Disk($Disks.Number)
  $Label 	= Get-VolumeTag($VolumeID) ('Drive_Label')
  $Letter =	Get-VolumeTag($VolumeID) ('Drive_Letter')

  if($VolumeID -like '*AWS-*' -AND ([string]::IsNullOrEmpty($Label.Value)))
  {
    $Label = @{}
    $Label.Add('Value',"AWS Ephemeral")
  }

  if (-not ([string]::IsNullOrEmpty($Letter.Value)))
  {
    Write-Host "Drive letter is " $Letter.Value
    New-Partition -DiskNumber $Disks.Number -UseMaximumSize -DriveLetter $Letter.Value | Format-Volume -FileSystem NTFS -NewFileSystemLabel $Label.Value -Confirm:$false
  }
  else{
  	Write-Host "No drive letter"
  	New-Partition -DiskNumber $Disks.Number -UseMaximumSize -AssignDriveLetter | Format-Volume -FileSystem NTFS -NewFileSystemLabel $Label.Value -Confirm:$false
  }
}
