

# Disk Initialise Script README

### Purpose
The purpose of this script is to automate the Initialisation of Disks on Windows Servers by utilising EBS volume tags to control certain aspects of the process.
It is a fully automated script which requires only one input parameter which is the -proxy flag which is further documented later on.

Overall this will help standardise how volumes are mounted and managed long term.


### Input Parameters
Parameter | Usage
----------- | -----------
Proxy    |   Needed for the script to work when server is behind a proxy, AWS CMDLETS don't use the default windows IE Proxy settings



### EBS Tags and functions

Tag | Usage
----------- | -----------
Drive_Letter | Used to assign a specific drive letter to the set Partition in windows, it can be left blank and Windows will assign one itself
Drive_Label | Used to assign a custom label to the Partition, If left blank windows will default to use 'Local Volume'


### Special Case AWS Ephemeral Volumes

In the case there are any available Ephemeral volumes when the instance launches the auto-disk-init script will auto mount them and label them as "AWS Ephemeral" with
randomly allocated Drive Letters.

WIP thesse disks should be handled last to ensure that required drive letter arent used by Ephemeral Disks when other EBS volumes with the tag Drive_Letter has been defined
