Function Get-Package-wget {
    [cmdletbinding()]
    Param (
        [string]$url,
        [string]$local_path
    )
    Process {
      (New-Object System.Net.WebClient).DownloadFile($url, $local_path)
    }
}

$puppet_installer_url = "https://binaries.avivagroup.com/artifactory/shared-installers/puppetlabs/puppet-agent/winx64_5.4.0/puppet-agent.msi"
$work_dir = "C:\temp"
$puppet_location = $work_dir + "\puppet-agent.msi"

#check if folder exsist
if(!(Test-Path -Path $work_dir )){
    New-Item -ItemType directory -Path $work_dir
}

Get-Package-wget -url $puppet_installer_url -local_path $puppet_location

$process = Start-Process -FilePath $puppet_location -ArgumentList "/qn" -PassThru -Wait

if ($process.ExitCode -eq 0)
{
	"puppet has installed successfully"
	exit 0
}
else
{
	"puppet has failed to install"
	exit 1
}