class role::bake_win_app {

  contain profile::windows::base
  contain profile::windows::download
  contain profile::windows::file
  contain profile::windows::chocolatey_package
  contain profile::windows::exec
  contain profile::windows::win_env

Class['profile::windows::base']
    -> Class['profile::windows::download']
      -> Class['profile::windows::file']
        -> Class['profile::windows::win_env']
          -> Class['profile::windows::exec']

}
