class role::run_lin_app {

  contain profile::linux::base
  contain profile::linux::download
  contain profile::linux::package
  contain profile::linux::file
  contain profile::linux::service
  contain profile::confluence::config_files

  Class['profile::linux::base']
    -> Class['profile::linux::file']
      -> Class['profile::confluence::config_files']
}
