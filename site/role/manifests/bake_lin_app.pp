class role::bake_lin_app {

  contain profile::linux::base
  contain profile::linux::download
  contain profile::linux::package
  contain profile::linux::file
  contain profile::linux::service
  contain profile::confluence::startup_scripts

  Class['profile::linux::base']
    -> Class['profile::linux::download']
      -> Class['profile::linux::package']
        -> Class['profile::confluence::startup_scripts']
          -> Class['profile::linux::service']

}
