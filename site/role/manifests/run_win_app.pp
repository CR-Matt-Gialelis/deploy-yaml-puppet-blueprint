class role::run_win_app {

  contain profile::windows::base
  contain profile::confluence::config_files
  contain profile::windows::file
  contain profile::windows::exec
  contain profile::windows::download


  Class['profile::windows::base']
    -> Class['profile::windows::file']
      -> Class['profile::confluence::config_files']
        -> Class['profile::windows::download']
          -> Class['profile::windows::exec']
}
