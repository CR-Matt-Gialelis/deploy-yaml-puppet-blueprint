# ENV["http_proxy"] ||= 'http://management-proxy.management.aws-euw1-np.avivacloud.com:80'
# ENV["https_proxy"] ||= 'http://management-proxy.management.aws-euw1-np.avivacloud.com:80'
# ENV["NO_PROXY"] ||= '127.0.0.1, localhost, 169.254.169.254'


  Facter.add('ec2tag_autoscaling_group_name') do
    setcode do

      instance_id = 'powershell.exe -Command "Invoke-RestMethod -uri http://169.254.169.254/latest/meta-data/instance-id"'
      instance_id = Facter::Core::Execution.exec(instance_id)

      cmd = "powershell.exe -Command \"aws ec2 describe-tags --filters Name=resource-id,Values=#{instance_id} --region eu-west-1 --query 'Tags[?Key==`aws:autoscaling:groupName`].Value[]' --output text\""
      output = Facter::Core::Execution.exec(cmd)

        unless output.to_s.strip.empty? || output.nil?
            output
          else
            puts "Orginal Output was..... #{output} ..... Ensure this is blank if errors are occuring"
            output = "instance_only"
            output
        end

    end
  end
