class profile::windows::win_task_scheduler {

  $scheduled_task = lookup('windows::windows_task_scheduler', {merge => hash, default_value => undef})

  if $scheduled_task!=undef {
    create_resources('scheduled_task', $scheduled_task)
  }

}
