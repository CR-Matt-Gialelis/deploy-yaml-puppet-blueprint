class profile::windows::win_registry {

  $values = lookup('profile::win_registry_values', {merge => hash, default_value => undef})
  $keys = lookup('profile::win_registry_keys', {merge => hash, default_value => undef})

  Registry_key<| |> -> Registry_value <| |>

  if $values!=undef {
    create_resources('registry_value', $values)
  }

  if $keys!=undef {
    create_resources('registry_key', $keys)
  }


}
