# == Class:
#
class profile::windows::exec {

  $exec = lookup(profile::windows::exec, {merge => hash, default_value => undef})

  if $exec!=undef {
    create_resources('exec', $exec)
  }

}
