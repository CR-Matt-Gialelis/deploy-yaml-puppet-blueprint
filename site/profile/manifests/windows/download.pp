# == Class:
#
class profile::windows::download {

  $download_links = lookup(profile::windows::download_links, {merge => hash, default_value => undef})

  if $download_links!=undef {
    create_resources('archive', $download_links)
  }

}
