# == Class: class_name
#
class profile::windows::file {

  $file = lookup(profile::windows::file, {merge => hash, default_value => undef})

  if $file!=undef {
    create_resources('file', $file)
  }

}
