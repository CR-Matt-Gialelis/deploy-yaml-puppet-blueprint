class profile::windows::chocolatey_package {

  $chocolatey_packages = lookup(chocolatey::packages, {merge => hash, default_value => undef})

  if $chocolatey_packages {
    create_resources('package', $chocolatey_packages )
  }

}
