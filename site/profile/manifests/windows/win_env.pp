class profile::windows::win_env {

  $values = lookup('profile::win_env', {merge => hash, default_value => undef})

  if $values!=undef {
    create_resources('windows_env', $values)
  }

}
