class profile::windows::install_windows_feature {

  $windows_feature = lookup(windows::windows_feature, {merge => hash, default_value => undef})
  $windows_feature_defaults = lookup(windows::windows_feature_defaults, {merge => hash, default_value => undef})
  $download_links = lookup(windows::download_links, {merge => hash, default_value => undef})

  # Used to order the create_resources below
  Archive<| |> -> Windowsfeature<| |>

  if $download_links!=undef {
    create_resources('archive', $download_links)
  }

  if $windows_feature!=undef {
    create_resources('windowsfeature', $windows_feature, $windows_feature_defaults )
  }

}
