class profile::windows::base {

  $local_path = lookup('common::local_path')
  $choco_install_url = lookup('common::choco_install_url', {default_value  => undef })
  $chocolatey_proxy = lookup('common::bakery_proxy', {default_value  => undef })

  file { $local_path :
    ensure => 'directory'
  }

  if $choco_install_url!=undef {
    class {'chocolatey':
      chocolatey_download_url => $choco_install_url,
      use_7zip                => false,
      log_output              => true,
    }
  }

  if $chocolatey_proxy!=undef {
    chocolateyconfig {'proxy':
      value  => $chocolatey_proxy,
    }
  }

  include '::archive'
}
