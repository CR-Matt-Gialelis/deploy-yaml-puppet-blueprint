class profile::windows::iis{

  $directories = lookup('profile::iis::directories', {merge => hash, default_value => undef})
  $pools = lookup('profile::iis::pools', {merge => hash, default_value => undef})
  $sites = lookup('profile::iis::sites', {merge => hash, default_value => undef})
  $apps = lookup('profile::iis::apps', {merge => hash, default_value => undef})

  # # Used to order the create_resources below
  Iis_application_pool<| |> -> Iis_site<| |> -> Iis_application<| |>

  if $directories!=undef {
    # Create App directories
    create_resources('file', $directories)
  }

  if $pools!=undef {
    # Create App Pools
    create_resources('iis_application_pool', $pools)
  }

  if $sites!=undef {
    # Create App Sites
    create_resources('iis_site', $sites)
  }

  if $apps!=undef {
    # Create Apps
    create_resources('iis_application', $apps)
  }

}
