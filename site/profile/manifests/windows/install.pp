# == Class:
#
class profile::windows::install {

  $packages = lookup(profile::windows::install, {merge => hash, default_value => undef})

  if $packages!=undef {
    create_resources('package', $packages)
  }

}
