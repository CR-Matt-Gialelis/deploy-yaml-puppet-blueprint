# == Class: class_name
#
class profile::linux::service {

  $service = lookup(profile::linux::service, {merge => hash, default_value => undef})

  if $service!=undef {
    create_resources('service', $service)
  }

}
