# == Class: class_name
#
class profile::linux::file {

  $file = lookup(profile::linux::file, {merge => hash, default_value => undef})

  if $file!=undef {
    create_resources('file', $file)
  }

}
