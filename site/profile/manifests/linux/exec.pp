# == Class:
#
class profile::linux::exec {

  $exec = lookup(profile::linux::exec, {merge => hash, default_value => undef})

  if $exec!=undef {
    create_resources('exec', $exec)
  }

}
