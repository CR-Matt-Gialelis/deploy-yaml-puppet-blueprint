class profile::linux::base {

  package { 'unzip':
    ensure => installed,
  }

  include '::archive'
}
