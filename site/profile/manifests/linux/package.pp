# == Class:
#
class profile::linux::package {

  $package = lookup(profile::linux::package, {merge => hash, default_value => undef})

  if $package!=undef {
    create_resources('package', $package)
  }

}
