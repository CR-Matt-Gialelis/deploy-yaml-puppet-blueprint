# Class: name
#
#
class profile::assoc::cloudwatch {


  exec { 'AmazonSSMStop':
    command  => 'Stop-Service "AmazonSSMAgent"',
    provider => powershell,
    timeout  => 60,
  }

  -> file { 'C:\Program Files\Amazon\SSM\Plugins\awsCloudWatch\AWS.EC2.Windows.CloudWatch.json':
    ensure  => file,
    content => template('profile/AWS.EC2.Windows.CloudWatch.json.erb'),
  }

  -> exec { 'AmazonSSMAgentRestart':
    command  => 'Start-Service "Amazon SSM Agent"',
    provider => powershell,
    timeout  => 60,
  }


}
