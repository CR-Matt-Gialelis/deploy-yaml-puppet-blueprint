
Param (
  [Parameter(Mandatory=$False)]  [String] $Role  =   $null,
  [Parameter(Mandatory=$False)]  [String] $Class  =   $null,
  [Parameter(Mandatory=$False)]  [String] $git_update   =   $null,
  [Parameter(Mandatory=$False)]  [String] $disk_init   =   $null,
  [Parameter(Mandatory=$False)]  [String] $db_password   =   $null,
  [Parameter(Mandatory=$False)]  [String] $db_url   =   $null,
  [Parameter(Mandatory=$False)]  [String] $db_port   =   $null

)

$Path = "C:\Aviva\Application\Provisioning"

if (Test-Path $path) {
  $code_location = $Path
  Write-Host "Default Aviva Puppet Code location Exists"
}
else {
  $code_location = (Get-location).Path
  Write-Host "Using Persent working directory of this script MAY CAUSE ISSUES"
}

# Demand and set Role
if ($Role) {
    $env:FACTER_role = $Role
    $env:FACTER_dbpassword = $db_password
    $env:FACTER_dburl = $db_url
    $env:FACTER_dbport = $db_port
}


# Run Disk Iniatilizsion script if True
If($disk_init){
  C:\Aviva\Application\Provisioning\scripts\auto-disk-init.ps1 -proxy http://management-proxy.management.aws-euw1-np.avivacloud.com
}

# If git update is set, Fetch and then Pull the repo again
If($git_update){
    # git config --global http.proxy "http://management-proxy.management.aws-euw1-np.avivacloud.com:80"
    # git config --global https.proxy "http://management-proxy.management.aws-euw1-np.avivacloud.com:80"
    git fetch
    git pull
}


if( -NOT ([string]::IsNullOrEmpty($role)) -AND ([string]::IsNullOrEmpty($class)) ) {
Write-Host "----------------------------------------------------------------------------------------------"
Write-Host "                                      Downloading Modules"
Write-Host "----------------------------------------------------------------------------------------------"

      puppet apply $code_location\get-puppet-moudles.pp `
      --debug --verbose `
      --logdest console `
      --logdest "$code_location\puppet_get_modules.log" | Out-Host

Write-Host "----------------------------------------------------------------------------------------------"
Write-Host "                                     Applying Puppet" 
Write-Host "                                        Role: $Role"
Write-Host "----------------------------------------------------------------------------------------------"

      puppet apply $code_location\manifests\site.pp `
      --modulepath="$code_location\site\;$code_location\modules\" `
      --hiera_config="$code_location\hiera.yaml" `
      --debug --verbose `
      --logdest console `
      --logdest "$code_location\puppetapply_$role.log" | Out-Host

}

elseif( -NOT ([string]::IsNullOrEmpty($class))){
  if (Test-Path "$code_location\puppetapply_class_run.log")
  {
    Remove-Item "$code_location\puppetapply_class_run.log"
  }
  Write-Host "----------------------------------------------------------------------------------------------"
  Write-Host "                                      Downloading Modules"
  Write-Host "----------------------------------------------------------------------------------------------"
  
        puppet apply $code_location\get-puppet-moudles.pp `
        --debug --verbose `
        --logdest console `
        --logdest "$code_location\puppet_get_modules.log" | Out-Host
  
Write-Host "----------------------------------------------------------------------------------------------"
Write-Host "                                     Applying Puppet" 
Write-Host "        This Job is only running the below Class the full puppet manifest is NOT being excuted"
Write-Host "                                        Role: $Role"
Write-Host "                                        Class: $Class"
Write-Host "----------------------------------------------------------------------------------------------"

  puppet apply $code_location\manifests\site.pp `
  --modulepath="$code_location\site\;$code_location\modules\" `
  --hiera_config="$code_location\hiera.yaml" `
  --debug --verbose `
  --logdest console `
  --logdest "$code_location\puppetapply_class_run.log" `
  -e "contain $class"
  
}
else{

Write-Host "----------------------------------------------------------------------------------------------"
Write-Host "                                     !!!!!NOTICE!!!!!" 
Write-Host "                   Puppet not excuted since both Role and Class Variable are empty"
Write-Host "----------------------------------------------------------------------------------------------"

}



