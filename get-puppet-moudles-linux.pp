

package { 'librarian-puppet':
ensure   => installed,
provider => puppet_gem,
}
exec { 'Install Required Puppet modules from Puppet file':
command => "librarian-puppet install --no-use-v1-api --verbose",
path    => '/opt/puppetlabs/puppet/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/opt/puppetlabs/bin:/opt/puppetlabs/puppet/lib/ruby/gems/2.4.0/gems/librarian-puppet-3.0.0/bin',
timeout => 1500,
}

