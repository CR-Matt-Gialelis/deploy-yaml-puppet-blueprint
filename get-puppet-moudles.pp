  #
  $powershell = 'C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe -NoProfile -NoLogo -NonInteractive'

  package { 'librarian-puppet':
    ensure   => installed,
    provider => puppet_gem,
  } ->
  file { 'Create Temp Dir for librarian':
    ensure    => 'directory',
    path      => 'c:\\tmp'
  } ->
  exec { 'Set Tmp Dir for librarian':
    command   => "${powershell} -Command \"&librarian-puppet config tmp c:/tmp --global\"",
    logoutput => true,
    timeout   => 1500,
  } ->
  exec { 'Install Required Puppet modules from Puppet file':
    command   => "${powershell} -Command \"&librarian-puppet install --no-use-v1-api --verbose\"",
    logoutput => true,
    timeout   => 1500,
  } ->
  exec { 'Remove Temp Dir for Librarian':
    command   => "${powershell} -Command \"Remove-Item -path 'c:\\tmp' -Recurse -Force -EA SilentlyContinue \"",
    logoutput => true,
    timeout   => 1500,
  }


